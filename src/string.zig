const std = @import("std");

const String = @This();

data: std.ArrayList(u8),

////////////////////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////////////////////

pub fn init(allocator: std.mem.Allocator) !String {
    return initCopy(allocator, "");
}

pub fn initCopy(allocator: std.mem.Allocator, str: []const u8) !String {
    var s: String = .{ .data = std.ArrayList(u8).init(allocator) };
    try s.data.resize(str.len + 1);
    std.mem.copy(u8, s.data.items, str);
    s.data.items[str.len] = 0;
    return s;
}

pub fn initFromCString(allocator: std.mem.Allocator, str: [*c]const u8) !String {
    var len: usize = 0;
    while (str[len] != 0) {
        len += 1;
    }
    return initCopy(allocator, str[0..len]);
}

pub fn deinit(self: *String) void {
    self.data.deinit();
}

////////////////////////////////////////////////////////////////////////////////
// Accessors
////////////////////////////////////////////////////////////////////////////////

pub fn asSlice(self: *const String) []const u8 {
    return self.data.items[0..(self.data.items.len - 1)];
}

pub fn asCSlice(self: *const String) [:0]const u8 {
    return self.data.items[0..(self.data.items.len - 1) :0];
}

pub fn getSize(self: *const String) usize {
    return self.data.items.len - 1;
}

pub fn eqlSlice(self: *const String, s: []const u8) bool {
    return std.mem.eql(u8, self.asSlice(), s);
}

////////////////////////////////////////////////////////////////////////////////
// Mutators
////////////////////////////////////////////////////////////////////////////////

pub fn clear(self: *String) void {
    self.data.shrinkRetainingCapacity(1);
    self.data.items[0] = 0;
}

pub fn prependSlice(self: *String, s: []const u8) !void {
    try self.data.insertSlice(0, s);
}

pub fn assignSlice(self: *String, s: []const u8) !void {
    self.clear();
    try self.prependSlice(s);
}

////////////////////////////////////////////////////////////////////////////////
// Tests
////////////////////////////////////////////////////////////////////////////////

test "From C string" {
    {
        const c_str: [*c]const u8 = "abc\x00def";
        var str = try String.initFromCString(std.testing.allocator, c_str);
        defer str.deinit();
        try std.testing.expectEqual(@as(usize, 3), str.getSize());
        try std.testing.expect(str.eqlSlice("abc"));
    }
}

test "Prepend" {
    {
        var str = try String.init(std.testing.allocator);
        defer str.deinit();
        try str.prependSlice("def");
        try std.testing.expect(str.eqlSlice("def"));
        try str.prependSlice("abc");
        try std.testing.expect(str.eqlSlice("abcdef"));
        try std.testing.expect(std.mem.eql(u8, str.asCSlice(), "abcdef"));
        try std.testing.expectEqual(@as(usize, 6), str.getSize());
    }
}
