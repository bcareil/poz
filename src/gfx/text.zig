const std = @import("std");
const gpu = @import("gpu");

const bm = @import("../bitmap.zig");
const Font = @import("../font.zig");
const FontManager = @import("../font_manager.zig");
const gfx = @import("../gfx.zig");
const String = @import("../string.zig");
const TextureManager = @import("../texture_manager.zig");
const types = @import("../types.zig");

const BitmapUnmanaged = bm.BitmapUnmanaged;
const DrawTextCommand = gfx.Commands.DrawTextCommand;
const FontId = FontManager.FontId;
const GlyphRef = Font.GlyphRef;
const SpriteBatch = gfx.SpriteBatch;
const BBox = types.BBox;

pub const Error = error{
    NotEnoughSpace,
};

pub const TextRenderer = struct {
    allocator: std.mem.Allocator,
    font_manager: *FontManager,
    texture_manager: *TextureManager,
    glyph_lookup: GlyphLookup,
    glyphs: std.MultiArrayList(GlyphEntry),
    texture: TextureManager.TextureRef,
    texture_buffer: []u8,
    texture_dirty: bool = false,
    next_column: u32 = 0,
    next_row: u32 = 0,
    current_row: u32 = 0,

    const Self = @This();

    const GlyphId = usize;
    const GlyphLookup = std.AutoArrayHashMap(GlyphKey, GlyphId);

    const GlyphKey = struct {
        codepoint: u32,
        font: FontId,
        size: u32,
    };

    const GlyphEntry = struct {
        bbox_uv: BBox(f32),
        pen_offset: @Vector(2, f32),
        pen_advance: @Vector(2, f32),
        dimensions: @Vector(2, f32),
    };

    const TextKey = struct {
        font: FontId,
        size: u32,
        text: String,
    };

    const UNMAPPED_GLYPH: u32 = 0xffffffff;
    const EMPTY_GLYPH: u32 = 0x20; // space

    pub fn init(font_manager: *FontManager, texture_manager: *TextureManager, allocator: std.mem.Allocator) !Self {
        const texture_size = @Vector(2, u32){ 512, 512 };
        const texture = try texture_manager.createTexture(texture_size[0], texture_size[1], null);
        errdefer texture_manager.release(texture.id);

        var texture_buffer = try allocator.alloc(u8, texture_size[0] * texture_size[1] * 4);
        errdefer allocator.free(texture_buffer);

        var self = Self{
            .allocator = allocator,
            .font_manager = font_manager,
            .texture_manager = texture_manager,
            .glyph_lookup = GlyphLookup.init(allocator),
            .glyphs = .{},
            .texture = texture,
            .texture_buffer = texture_buffer,
        };

        var unmapped_glyph: []u8 = try allocator.dupe(u8, &[_]u8{
            0x00, 0x00, 0x00, 0x00,
            0x00, 0xff, 0xff, 0x00,
            0x00, 0xff, 0xff, 0x00,
            0x00, 0x00, 0x00, 0x00,
        });

        _ = try self.loadGlyph(
            .{
                .codepoint = UNMAPPED_GLYPH,
                .font = 0,
                .size = 0,
            },
            .{
                .pixels = BitmapUnmanaged(u8).init(unmapped_glyph, 4, 4),
                .offsets = .{
                    .offset = .{ 0, 4 },
                    .advance = .{ 4, 0 },
                },
            },
        );

        return self;
    }

    pub fn deinit(self: *Self) void {
        self.texture_manager.release(self.texture.id);
        self.allocator.free(self.texture_buffer);
        self.glyph_lookup.deinit();
    }

    pub fn render(self: *Self, sprite_batch: *SpriteBatch, cmd: *const DrawTextCommand) !void {
        for (cmd.text.asSlice()) |t| {
            _ = self.getGlyph(t, cmd.font_family, cmd.font_size);
        }

        var pen = cmd.position;
        const glyphs_slice = self.glyphs.slice();
        const glyphs_bbox_uv = glyphs_slice.items(.bbox_uv);
        const glyphs_pen_offset = glyphs_slice.items(.pen_offset);
        const glyphs_pen_advance = glyphs_slice.items(.pen_advance);
        const glyphs_dimensions = glyphs_slice.items(.dimensions);
        for (cmd.text.asSlice()) |t| {
            const glyph_id = self.getGlyph(t, cmd.font_family, cmd.font_size);
            const p = pen + glyphs_pen_offset[glyph_id];
            const d = glyphs_dimensions[glyph_id];
            const uv = glyphs_bbox_uv[glyph_id];
            try sprite_batch.pushSpritePDUV(self.texture.id, p, d, uv);
            pen += glyphs_pen_advance[glyph_id];
        }
    }

    pub fn preFrame(self: *Self, texture_manager: *TextureManager) void {
        if (self.texture_dirty) {
            texture_manager.updateTexture(self.texture.id, self.texture_buffer);
            self.texture_dirty = false;
        }
    }

    fn getGlyph(self: *Self, codepoint: u32, font_id: FontId, size: u32) GlyphId {
        return self.tryGetGlyph(codepoint, font_id, size) catch 0;
    }

    fn tryGetGlyph(self: *Self, codepoint: u32, font_id: FontId, size: u32) !GlyphId {
        const key = GlyphKey{
            .codepoint = codepoint,
            .font = font_id,
            .size = size,
        };
        var entry = try self.glyph_lookup.getOrPut(key);
        if (entry.found_existing) {
            return entry.value_ptr.*;
        }
        errdefer _ = self.glyph_lookup.swapRemove(key);

        const font = self.font_manager.get(font_id) orelse return error.InvalidFontId;
        const glyph = try font.getGlyph(codepoint, size);

        try self.loadGlyphImpl(entry, &glyph);

        return entry.value_ptr.*;
    }

    fn loadGlyph(self: *Self, key: GlyphKey, glyph: GlyphRef) !void {
        var entry = try self.glyph_lookup.getOrPut(key);
        if (entry.found_existing) {
            return error.AlreadyExist;
        }
        errdefer _ = self.glyph_lookup.swapRemove(key);
        try self.loadGlyphImpl(entry, &glyph);
    }

    fn loadGlyphImpl(self: *Self, entry_no_value: GlyphLookup.GetOrPutResult, glyph: *const GlyphRef) !void {
        var current_column = self.next_column;
        var end_next_column = @intCast(u32, self.next_column + glyph.pixels.width);
        var end_current_row = self.current_row;
        var end_next_row = @intCast(u32, @maximum(self.next_row, self.current_row + glyph.pixels.height));
        if (end_next_column >= self.texture.size.w) {
            current_column = 0;
            end_next_column = @intCast(u32, glyph.pixels.width);
            end_current_row = self.next_row;
            end_next_row = end_current_row + @intCast(u32, glyph.pixels.height);
        }
        if (end_next_column >= self.texture.size.w or end_next_row >= self.texture.size.h) {
            return Error.NotEnoughSpace;
        }

        const texture_w = @intToFloat(f32, self.texture.size.w);
        const texture_h = @intToFloat(f32, self.texture.size.h);
        const bbox = BBox(f32){
            .left = @intToFloat(f32, current_column) / texture_w,
            .top = @intToFloat(f32, end_current_row) / texture_h,
            .right = @intToFloat(f32, end_next_column) / texture_w,
            .bottom = @intToFloat(f32, end_current_row + glyph.pixels.height) / texture_h,
        };

        entry_no_value.value_ptr.* = self.glyphs.len;
        try self.glyphs.append(self.allocator, .{
            .bbox_uv = bbox,
            .pen_offset = .{
                glyph.offsets.offset[0],
                -glyph.offsets.offset[1],
            },
            .pen_advance = glyph.offsets.advance,
            .dimensions = .{
                @intToFloat(f32, glyph.pixels.width),
                @intToFloat(f32, glyph.pixels.height),
            },
        });

        self.blit(glyph.pixels, current_column, end_current_row);

        // commit the changes
        self.next_column = end_next_column;
        self.current_row = end_current_row;
        self.next_row = end_next_row;
    }

    fn blit(self: *Self, bitmap: BitmapUnmanaged(u8), tex_x: u32, tex_y: u32) void {
        self.texture_dirty = true;
        var y: usize = 0;
        while (y < bitmap.height) {
            var x: usize = 0;
            var ti = (tex_x + (tex_y + y) * self.texture.size.w) * 4;
            while (x < bitmap.width) {
                const p = bitmap.get(x, y);
                self.texture_buffer[ti + 0] = p;
                self.texture_buffer[ti + 1] = p;
                self.texture_buffer[ti + 2] = p;
                self.texture_buffer[ti + 3] = p;
                x += 1;
                ti += 4;
            }
            y += 1;
        }
    }
};

test {
    _ = TextRenderer;
}
