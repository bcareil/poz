const std = @import("std");
const gpu = @import("gpu");
const zigimg = @import("zigimg");

const types = @import("types.zig");
const String = @import("string.zig");

const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;

const Dimensions = types.Dimensions;

pub const TextureRef = struct {
    id: TextureManager.TexId,
    path: []const u8,
    size: Dimensions,
};

pub const TextureEntry = struct {
    ref_count: u32,
    path: String,
    size: Dimensions,
    texture: gpu.Texture,
};

const TextureManager = @This();

allocator: Allocator,
device: *gpu.Device,
default_tex_ref: TextureRef,
lookup: std.StringHashMap(TexId),
textures: ArrayList(TextureEntry),

pub const TexId = u32;

pub fn init(allocator: Allocator, device: *gpu.Device) !TextureManager {
    var textures = try ArrayList(TextureEntry).initCapacity(allocator, 128);
    errdefer textures.deinit();

    var tex_man = TextureManager{
        .allocator = allocator,
        .device = device,
        .lookup = std.StringHashMap(TexId).init(allocator),
        .textures = textures,
        .default_tex_ref = .{ // will be initialized properly later on
            .id = 0,
            .path = "",
            .size = .{ .w = 1, .h = 1 },
        },
    };
    errdefer tex_man.lookup.deinit();

    var desc: gpu.Texture.Descriptor = .{
        .usage = .{
            .copy_dst = true,
            .texture_binding = true,
            .render_attachment = true,
        },
        .dimension = .dimension_2d,
        .size = .{ .width = 1, .height = 1, .depth_or_array_layers = 1 },
        .format = .rgba8_unorm,
    };
    var default_tex = device.createTexture(&desc);
    errdefer default_tex.destroy();

    var pixel: []const u8 = &[4]u8{ 0xff, 0xff, 0xff, 0xff };
    device.getQueue().writeTexture(
        &.{ .texture = default_tex },
        pixel,
        &.{
            .bytes_per_row = 4,
            .rows_per_image = 1,
        },
        &.{
            .width = 1,
            .height = 1,
        },
    );

    var path = try String.init(allocator);
    errdefer path.deinit();

    try tex_man.textures.append(.{
        .ref_count = 1,
        .path = path,
        .size = .{ .w = 1, .h = 1 },
        .texture = default_tex,
    });

    tex_man.default_tex_ref = tex_man.createRef(0);

    return tex_man;
}

pub fn createTexture(self: *TextureManager, width: u32, height: u32, path: ?[]const u8) !TextureRef {
    const index = @intCast(TexId, self.textures.items.len);
    var path_str = if (path) |p| try String.initCopy(self.allocator, p) else try String.init(self.allocator);
    errdefer path_str.deinit();
    if (path) |_| {
        try self.lookup.put(path_str.asSlice(), index);
    }
    errdefer if (path) |_| {
        _ = self.lookup.remove(path_str.asSlice());
    };
    var desc: gpu.Texture.Descriptor = .{
        .usage = .{
            .copy_dst = true,
            .texture_binding = true,
            .render_attachment = true,
        },
        .dimension = .dimension_2d,
        .size = .{
            .width = width,
            .height = height,
            .depth_or_array_layers = 1,
        },
        .format = .rgba8_unorm,
    };
    var tex = self.device.createTexture(&desc);
    errdefer tex.destroy();
    try self.textures.append(.{
        .ref_count = 0,
        .path = path_str,
        .size = .{
            .w = width,
            .h = height,
        },
        .texture = tex,
    });
    errdefer self.textures.shrinkRetainingCapacity(index);
    return self.createRef(index);
}

pub fn updateTexture(self: *TextureManager, id: TexId, data: anytype) void {
    const texture = &self.textures.items[id];
    var dst: gpu.ImageCopyTexture = .{
        .texture = texture.texture,
        .mip_level = 0,
        .origin = .{ .x = 0, .y = 0, .z = 0 },
        .aspect = .all,
    };
    var extent: gpu.Extent3D = .{
        .width = texture.size.w,
        .height = texture.size.h,
        .depth_or_array_layers = 1,
    };
    var layout: gpu.Texture.DataLayout = .{
        .offset = 0,
        .bytes_per_row = 4 * extent.width,
        .rows_per_image = extent.height,
    };
    self.device.getQueue().writeTexture(&dst, data, &layout, &extent);
}

fn loadRgba32(
    self: *TextureManager,
    path: []const u8,
    data: []const zigimg.color.Rgba32,
    width: u32,
    height: u32,
) !TextureRef {
    const ref = try self.createTexture(width, height, path);
    const texture = &self.textures.items[ref.id];
    errdefer self.deleteTexture(ref.id);
    var dst: gpu.ImageCopyTexture = .{
        .texture = texture.texture,
        .mip_level = 0,
        .origin = .{ .x = 0, .y = 0, .z = 0 },
        .aspect = .all,
    };
    var layout: gpu.Texture.DataLayout = .{
        .offset = 0,
        .bytes_per_row = 4 * width,
        .rows_per_image = height,
    };
    var extent: gpu.Extent3D = .{
        .width = width,
        .height = height,
        .depth_or_array_layers = 1,
    };
    self.device.getQueue().writeTexture(&dst, data, &layout, &extent);
    return ref;
}

fn loadImpl(self: *TextureManager, path: []const u8) TextureRef {
    if (zigimg.image.Image.fromFilePath(self.allocator, path)) |img| {
        defer img.deinit();
        if (img.pixels) |pixels| {
            if (pixels == .Rgba32) {
                if (self.loadRgba32(path, pixels.Rgba32, @intCast(u32, img.width), @intCast(u32, img.height))) |ref| {
                    return ref;
                } else |err| {
                    std.log.info("Could not load {s}: {}", .{ path, err });
                }
            } else {
                std.log.info("Could not load {s}: not rgba32", .{path});
            }
        } else {
            std.log.info("Could not load {s}: no pixels ?!", .{path});
        }
    } else |err| {
        std.log.info("Could not load {s}: {}", .{ path, err });
    }
    return self.default_tex_ref;
}

pub fn load(self: *TextureManager, path: []const u8) TextureRef {
    if (self.lookup.get(path)) |index| {
        return self.createRef(index);
    }
    return self.loadImpl(path);
}

fn createRef(self: *TextureManager, index: TexId) TextureRef {
    const tex = self.get(index);
    return .{
        .id = @intCast(u32, index),
        .path = tex.path.asSlice(),
        .size = tex.size,
    };
}

pub fn getNoDefault(self: *TextureManager, index: TexId) ?*const TextureEntry {
    if (index >= self.textures.items.len) {
        return null;
    }
    return &self.textures.items[index];
}

pub fn get(self: *TextureManager, index: TexId) *const TextureEntry {
    if (self.getNoDefault(index)) |te| {
        return te;
    }
    if (self.textures.items.len == 0) {
        @panic("No textures loaded at all");
    }
    return self.get(0);
}

pub fn getDefaultTexture(self: *TextureManager) gpu.Texture {
    return self.get(0).texture;
}

pub fn release(self: *TextureManager, id: TexId) void {
    if (id == 0) return;
    if (id >= self.textures.items.len) return;
    var tex = &self.textures.items[id];
    tex.ref_count -= 1;
    if (tex.ref_count == 0) {
        // TODO
    }
}

fn deleteTexture(self: *TextureManager, id: TexId) void {
    {
        var texture = &self.textures.items[id];
        texture.texture.destroy();
        texture.path.deinit();
    }
    self.textures.shrinkRetainingCapacity(id);
}

pub fn deinit(self: *TextureManager) void {
    while (self.textures.items.len > 0) {
        self.deleteTexture(@intCast(TexId, self.textures.items.len - 1));
    }
    self.textures.deinit();
    self.lookup.deinit();
}

test {
    _ = TextureManager;
}
