const std = @import("std");
const lj = @import("luajit");
const mach = @import("mach");
const gpu = @import("gpu");

const bind = @import("lua_api/bind.zig");
const FontManager = @import("font_manager.zig");
const gfx = @import("gfx.zig");
const String = @import("string.zig");
const TextureManager = @import("texture_manager.zig");
const lua_ctx = @import("lua_ctx.zig");

const Dimensions = @import("types.zig").Dimensions;
const LuaContext = lua_ctx.LuaContext;

const ctx = &lua_ctx.ctx;

const App = @This();

gpa: std.heap.GeneralPurposeAllocator(.{}),
pipeline: gpu.RenderPipeline,
queue: gpu.Queue,
renderer: gfx.Renderer,

pub fn init(app: *App, engine: *mach.Engine) !void {
    const vs_module = engine.gpu_driver.device.createShaderModule(&.{
        .label = "my vertex shader",
        .code = .{ .wgsl = @embedFile("vert.wgsl") },
    });

    const fs_module = engine.gpu_driver.device.createShaderModule(&.{
        .label = "my fragment shader",
        .code = .{ .wgsl = @embedFile("frag.wgsl") },
    });

    app.gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var allocator = app.gpa.allocator();

    {
        const args = try std.process.argsAlloc(allocator);
        defer std.process.argsFree(allocator, args);
        if (args.len < 2) {
            // pass
        } else if (args.len == 2) {
            try std.os.chdir(args[1]);
        } else {
            std.log.err("Invalid argument", .{});
        }
    }

    ctx.* = LuaContext{
        .allocator = allocator,
        .lua = lj.c.luaL_newstate() orelse {
            return error.LuaInitError;
        },
        .font_manager = undefined,
        .texture_manager = undefined,
        .screen_size = .{ .w = 640, .h = 480 },
        .commands = gfx.Commands.init(allocator),
    };
    errdefer {
        lj.c.lua_close(ctx.lua);
        ctx.commands.deinit();
    }

    ctx.font_manager = try FontManager.init(allocator);
    errdefer ctx.font_manager.deinit();
    _ = try ctx.font_manager.load("DejaVuSans", "fonts/DejaVuSans.ttf");

    // Fragment state
    const blend = gpu.BlendState{
        .color = .{
            .operation = .add,
            .src_factor = .one,
            .dst_factor = .one,
        },
        .alpha = .{
            .operation = .add,
            .src_factor = .one,
            .dst_factor = .one,
        },
    };
    const color_target = gpu.ColorTargetState{
        .format = engine.gpu_driver.swap_chain_format,
        .blend = &blend,
    };
    const fragment = gpu.FragmentState{
        .module = fs_module,
        .entry_point = "main",
        .targets = &.{color_target},
        .constants = null,
    };

    // Pipeline
    const vertex_attributes = [_]gpu.VertexAttribute{
        .{
            .format = .float32x4,
            .offset = @offsetOf(gfx.Vertex, "pos"),
            .shader_location = 0,
        },
        .{
            .format = .float32x2,
            .offset = @offsetOf(gfx.Vertex, "uv"),
            .shader_location = 1,
        },
    };
    const vertex_buffer_layout: gpu.VertexBufferLayout = .{
        .array_stride = @sizeOf(gfx.Vertex),
        .step_mode = .vertex,
        .attribute_count = vertex_attributes.len,
        .attributes = &vertex_attributes,
    };
    const pipeline_descriptor = gpu.RenderPipeline.Descriptor{
        .fragment = &fragment,
        .layout = null,
        .depth_stencil = null,
        .vertex = .{
            .module = vs_module,
            .entry_point = "main",
            .buffers = &.{vertex_buffer_layout},
        },
        .multisample = .{
            .count = 1,
            .mask = 0xFFFFFFFF,
            .alpha_to_coverage_enabled = false,
        },
        .primitive = .{
            .front_face = .ccw,
            .cull_mode = .none,
            .topology = .triangle_list,
            .strip_index_format = .none,
        },
    };
    app.pipeline = engine.gpu_driver.device.createRenderPipeline(&pipeline_descriptor);

    ctx.texture_manager = try TextureManager.init(allocator, &engine.gpu_driver.device);
    errdefer ctx.texture_manager.deinit();

    app.renderer = try gfx.Renderer.init(&engine.gpu_driver.device, &app.pipeline, &ctx.texture_manager, &ctx.font_manager, allocator);
    errdefer app.renderer.deinit();

    lj.c.luaL_openlibs(ctx.lua);
    try bind.bindLuaCallbacks(ctx.lua);
    try bind.bindCommonLuaCallbacks(ctx.lua, allocator);

    var err = lj.c.luaL_loadfile(ctx.lua, "PathOfBuilding/src/Launch.lua");
    if (err != 0) {
        _ = lj.c.lua_error(ctx.lua);
    }

    err = lj.c.lua_pcall(ctx.lua, 0, lj.c.LUA_MULTRET, 0);
    if (err != 0) {
        _ = lj.c.lua_error(ctx.lua);
    }

    lj.c.lua_getfield(ctx.lua, lj.c.LUA_REGISTRYINDEX, "uicallbacks");
    lj.c.lua_getfield(ctx.lua, -1, "MainObject");
    lj.c.lua_remove(ctx.lua, -2);
    lj.c.lua_getfield(ctx.lua, -1, "OnInit");
    lj.c.lua_insert(ctx.lua, -2);

    err = lj.c.lua_pcall(ctx.lua, 1, 0, 0);
    if (err != 0) {
        _ = lj.c.lua_error(ctx.lua);
    }

    vs_module.release();
    fs_module.release();
}

pub fn deinit(app: *App, _: *mach.Engine) void {
    lj.c.lua_close(ctx.lua);
    app.renderer.deinit();
    ctx.texture_manager.deinit();
    ctx.font_manager.deinit();
}

pub fn update(app: *App, engine: *mach.Engine) !bool {
    ctx.commands.clear();
    //ctx.lua.run("draw()");
    try ctx.commands.addDrawImage(.{
        .texture = 0,
        .position = .{ 0, 0 },
        .size = .{ 100, 100 },
    });
    try ctx.commands.addDrawImage(.{
        .texture = 1,
        .position = .{ 75, 75 },
        .size = .{ 100, 100 },
    });
    try ctx.commands.addDrawImage(.{
        .texture = 1,
        .position = .{ 150, 150 },
        .size = .{ 150, 150 },
    });
    try ctx.commands.addDrawText(.{
        .text = try String.initCopy(ctx.allocator, "Hello world"),
        .alignment = .Left,
        .position = .{ 132.0, 148.0 },
        .font_size = 16,
        .font_family = 0,
    });

    const back_buffer_view = engine.gpu_driver.swap_chain.?.getCurrentTextureView();
    const color_attachment = gpu.RenderPassColorAttachment{
        .view = back_buffer_view,
        .resolve_target = null,
        .clear_value = std.mem.zeroes(gpu.Color),
        .load_op = .clear,
        .store_op = .store,
    };

    const encoder = engine.gpu_driver.device.createCommandEncoder(null);
    const render_pass_info = gpu.RenderPassEncoder.Descriptor{
        .color_attachments = &.{color_attachment},
        .depth_stencil_attachment = null,
    };
    var pass = encoder.beginRenderPass(&render_pass_info);
    pass.setPipeline(app.pipeline);

    app.renderer.render(&app.pipeline, &engine.gpu_driver.device, &pass, &ctx.texture_manager, &ctx.commands);

    pass.end();
    pass.release();

    var command = encoder.finish(null);
    encoder.release();

    engine.gpu_driver.device.getQueue().submit(&.{command});
    command.release();
    engine.gpu_driver.swap_chain.?.present();
    back_buffer_view.release();

    return true;
}
