const std = @import("std");
const lj = @import("luajit");

const gfx = @import("gfx.zig");
const types = @import("types.zig");
const FontManager = @import("font_manager.zig");
const TextureManager = @import("texture_manager.zig");

const Dimensions = types.Dimensions;

pub const LuaContext = struct {
    allocator: std.mem.Allocator,
    lua: *lj.c.lua_State,
    font_manager: FontManager,
    texture_manager: TextureManager,
    screen_size: Dimensions,
    commands: gfx.Commands,
};

pub var ctx: LuaContext = undefined;
