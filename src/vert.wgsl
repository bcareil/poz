struct Uniforms {
  modelViewProjectionMatrix : mat4x4<f32>,
};
@binding(0) @group(0) var<uniform> uniforms : Uniforms;

struct VertexOut {
     @builtin(position) position: vec4<f32>,
     @location(0) fragUV : vec2<f32>,
     @location(1) fragPosition : vec4<f32>,
}

@stage(vertex) fn main(
    @location(0) position: vec4<f32>,
    @location(1) uv: vec2<f32>,
) -> VertexOut {
    var output: VertexOut;
    output.position = position * uniforms.modelViewProjectionMatrix;
    output.fragUV = uv;
    output.fragPosition = 0.5 * (output.position + vec4<f32>(1.0, 1.0, 1.0, 1.0));
    output.fragPosition.a = 1.0;
    return output;
}
