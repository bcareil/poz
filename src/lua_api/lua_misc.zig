const lj = @import("luajit");

pub fn lSetCallback(L: *lj.c.lua_State) callconv(.C) c_int {
    const n = lj.c.lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: SetCallback(name[, func])");
    //LAssert(L, lua_isstring(L, 1), "SetCallback() argument 1: expected string, got %t", 1);
    lj.c.lua_pushvalue(L, 1);
    if (n >= 2) {
        //LAssert(L, lua_isfunction(L, 2) || lua_isnil(L, 2), "SetCallback() argument 2: expected function or nil, got %t", 2);
        lj.c.lua_pushvalue(L, 2);
    } else {
        lj.c.lua_pushnil(L);
    }
    lj.c.lua_settable(L, lj.c.lua_upvalueindex(1));
    return 0;
}

pub fn lGetCallback(L: *lj.c.lua_State) callconv(.C) c_int {
    //const n = lj.c.lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: GetCallback(name)");
    //LAssert(L, lua_isstring(L, 1), "GetCallback() argument 1: expected string, got %t", 1);
    lj.c.lua_pushvalue(L, 1);
    lj.c.lua_gettable(L, lj.c.lua_upvalueindex(1));
    return 1;
}

pub fn lSetMainObject(L: *lj.c.lua_State) callconv(.C) c_int {
    const n = lj.c.lua_gettop(L);
    lj.c.lua_pushstring(L, "MainObject");
    if (n >= 1) {
        //LAssert(L, lua_istable(L, 1) || lua_isnil(L, 1), "SetMainObject() argument 1: expected table or nil, got %t", 1);
        lj.c.lua_pushvalue(L, 1);
    } else {
        lj.c.lua_pushnil(L);
    }
    lj.c.lua_settable(L, lj.c.lua_upvalueindex(1));
    return 0;
}

pub fn lSetWindowTitle(L: *lj.c.lua_State) callconv(.C) c_int {
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: SetWindowTitle(title)");
    //LAssert(L, lua_isstring(L, 1), "SetWindowTitle() argument 1: expected string, got %t", 1);
    //pobwindow->setTitle(lua_tostring(L, 1));
    _ = L;
    return 0;
}

pub fn lGetScriptPath(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //lua_pushstring(L, pobwindow->scriptPath.toStdString().c_str());
    //return 1;
}

pub fn lGetRuntimePath(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //lua_pushstring(L, pobwindow->basePath.toStdString().c_str());
    //return 1;
}

pub fn lGetUserPath(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //lua_pushstring(L, pobwindow->userPath.toStdString().c_str());
    //return 1;
}

pub fn lLoadModule(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: LoadModule(name[, ...])");
    //LAssert(L, lua_isstring(L, 1), "LoadModule() argument 1: expected string, got %t", 1);
    //QString fileName(lua_tostring(L, 1));
    //if (!fileName.endsWith(".lua")) {
    //    fileName = fileName + ".lua";
    //}
    //QDir::setCurrent(pobwindow->scriptPath);
    //int err = luaL_loadfile(L, fileName.toStdString().c_str());
    //QDir::setCurrent(pobwindow->scriptWorkDir);
    //LAssert(L, err == 0, "LoadModule() error loading '%s':\n%s", fileName.toStdString().c_str(), lua_tostring(L, -1));
    //lua_replace(L, 1);	// Replace module name with module main chunk
    //lua_call(L, n - 1, LUA_MULTRET);
    //return lua_gettop(L);
}

pub fn lPLoadModule(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    // int n = lua_gettop(L);
    // LAssert(L, n >= 1, "Usage: PLoadModule(name[, ...])");
    // LAssert(L, lua_isstring(L, 1), "PLoadModule() argument 1: expected string, got %t", 1);
    // QString fileName(lua_tostring(L, 1));
    // if (!fileName.endsWith(".lua")) {
    //     fileName = fileName + ".lua";
    // }
    // QDir::setCurrent(pobwindow->scriptPath);
    // int err = luaL_loadfile(L, fileName.toStdString().c_str());
    // QDir::setCurrent(pobwindow->scriptWorkDir);
    // if (err) {
    //     return 1;
    // }
    // lua_replace(L, 1);	// Replace module name with module main chunk
    // //lua_getfield(L, LUA_REGISTRYINDEX, "traceback");
    // //lua_insert(L, 1); // Insert traceback function at start of stack
    // err = lua_pcall(L, n - 1, LUA_MULTRET, 0);
    // if (err) {
    //     return 1;
    // }
    // lua_pushnil(L);
    // lua_insert(L, 1);
    //  //   lua_replace(L, 1); // Replace traceback function with nil
    // return lua_gettop(L);
}

pub fn lPCall(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    // int n = lua_gettop(L);
    // LAssert(L, n >= 1, "Usage: PCall(func[, ...])");
    // LAssert(L, lua_isfunction(L, 1), "PCall() argument 1: expected function, got %t", 1);
    // lua_pushcfunction(L, l_pcallErrorHandler);
    // lua_insert(L, 1);
    // int err = lua_pcall(L, n - 1, LUA_MULTRET, 1);
    // lua_remove(L, 1);
    // if (err) {
    //     std::cout << "PCall error: " << describeLuaError(err) << "\n";
    //     if (lua_isstring(L, 1)) {
    //       std::cout << lua_tostring(L, 1) << "\n";
    //     }
    //     invokeLuaDebugTraceback(L);
    //     return 1;
    // }
    // // no error is represented as nil in the first member of the returned tuple
    // lua_pushnil(L);
    // lua_insert(L, 1);
    // return lua_gettop(L);
}

pub fn lConPrintf(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: ConPrintf(fmt[, ...])");
    //LAssert(L, lua_isstring(L, 1), "ConPrintf() argument 1: expected string, got %t", 1);
    //lua_pushvalue(L, lua_upvalueindex(1));	// string.format
    //lua_insert(L, 1);
    //lua_call(L, n, 1);
    //LAssert(L, lua_isstring(L, 1), "ConPrintf() error: string.format returned non-string");
    //std::cout << lua_tostring(L, 1) << std::endl;
    ////pobwindow->sys->con->Printf("%s\n", lua_tostring(L, 1));
    //return 0;
}

pub fn lPrintTableItter(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //lua_checkstack(L, 5);
    //lua_pushnil(L);
    //while (lua_next(L, index)) {
    //    for (int t = 0; t < level; t++) std::cout << "  ";
    //    // Print key
    //    if (lua_type(L, -2) == LUA_TSTRING) {
    //        std::cout << "[\"" << lua_tostring(L, -2) << "\"] = ";
    //    } else {
    //        lua_pushvalue(L, 2);	// Push tostring function
    //        lua_pushvalue(L, -3);	// Push key
    //        lua_call(L, 1, 1);		// Call tostring
    //        std::cout << lua_tostring(L, -1) << " = ";
    //        lua_pop(L, 1);			// Pop result of tostring
    //    }
    //    // Print value
    //    if (lua_type(L, -1) == LUA_TTABLE) {
    //        bool expand = recurse;
    //        if (expand) {
    //            lua_pushvalue(L, -1);	// Push value
    //            lua_gettable(L, 3);		// Index printed tables list
    //            expand = lua_toboolean(L, -1) == 0;
    //            lua_pop(L, 1);			// Pop result of indexing
    //        }
    //        if (expand) {
    //            lua_pushvalue(L, -1);	// Push value
    //            lua_pushboolean(L, 1);
    //            lua_settable(L, 3);		// Add to printed tables list
    //            std::cout << "table: " << lua_topointer(L, -1) << " {"
    //                      << std::endl;
    //            printTableItter(L, lua_gettop(L), level + 1, true);
    //            for (int t = 0; t < level; t++) std::cout << "  ";
    //            std::cout << "}" << std::endl;
    //        } else {
    //            std::cout << "table: " << lua_topointer(L, -1) << " { ... }\n";
    //        }
    //    } else if (lua_type(L, -1) == LUA_TSTRING) {
    //        std::cout << "\"" << lua_tostring(L, -1) << "\"" << std::endl;
    //    } else {
    //        lua_pushvalue(L, 2);	// Push tostring function
    //        lua_pushvalue(L, -2);	// Push value
    //        lua_call(L, 1, 1);		// Call tostring
    //        std::cout << lua_tostring(L, -1) << std::endl;
    //        lua_pop(L, 1);			// Pop result of tostring
    //    }
    //    lua_pop(L, 1);	// Pop value
    //}
}

pub fn lConPrintTable(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: ConPrintTable(tbl[, noRecurse])");
    //LAssert(L, lua_istable(L, 1), "ConPrintTable() argument 1: expected table, got %t", 1);
    //bool recurse = lua_toboolean(L, 2) == 0;
    //lua_settop(L, 1);
    //lua_getglobal(L, "tostring");
    //lua_newtable(L);		// Printed tables list
    //lua_pushvalue(L, 1);	// Push root table
    //lua_pushboolean(L, 1);
    //lua_settable(L, 3);		// Add root table to printed tables list
    //printTableItter(L, 1, 0, recurse);
    //return 0;
}

pub fn lConExecute(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: ConExecute(cmd)");
    //LAssert(L, lua_isstring(L, 1), "ConExecute() argument 1: expected string, got %t", 1);
    ////pobwindow->sys->con->Execute(lua_tostring(L,1)); // FIXME
    //return 0;
}

pub fn lConClear(L: *lj.c.lua_State) callconv(.C) c_int {
    //    pobwindow->sys->con->Clear();
    _ = L;
    return 0;
}

pub fn lPrint(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //lua_getglobal(L, "tostring");
    //for (int i = 1; i <= n; i++) {
    //    lua_pushvalue(L, -1);	// Push tostring function
    //    lua_pushvalue(L, i);
    //    lua_call(L, 1, 1);		// Call tostring
    //    const char* s = lua_tostring(L, -1);
    //    LAssert(L, s != NULL, "print() error: tostring returned non-string");
    //    if (i > 1) std::cout << " ";
    //    std::cout << s;
    //    lua_pop(L, 1);			// Pop result of tostring
    //}
    //std::cout << std::endl;
    //return 0;
}

pub fn lSetProfiling(L: *lj.c.lua_State) callconv(.C) c_int {
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: SetProfiling(isEnabled)");
    //// FIXME
    ////pobwindow->debug->SetProfiling(lua_toboolean(L, 1) == 1);
    //return 0;
    _ = L;
    return 0;
}
