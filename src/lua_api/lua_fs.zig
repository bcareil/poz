const std = @import("std");
const lj = @import("luajit");

pub fn lNewFileSearch(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSearchHandleGC(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSearchHandleNextFile(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSearchHandleGetFileName(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSearchHandleGetFileSize(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSearchHandleGetFileModifiedTime(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lMakeDir(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: MakeDir(path)");
    //LAssert(L, lua_isstring(L, 1), "MakeDir() argument 1: expected string, got %t", 1);
    //lua_pushboolean(L, QDir().mkpath(lua_tostring(L, 1)));
    //return 1;
}

pub fn lRemoveDir(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: l_RemoveDir(path)");
    //LAssert(L, lua_isstring(L, 1), "l_RemoveDir() argument 1: expected string, got %t", 1);
    //QDir d;
    //if (!d.rmdir(lua_tostring(L, 1))) {
    //    lua_pushnil(L);
    //    return 1;
    //} else {
    //    lua_pushboolean(L, true);
    //    return 1;
    //}
}
