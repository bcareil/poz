const lj = @import("luajit");

pub fn lGetCursorPos(L: *lj.c.lua_State) callconv(.C) c_int {
    //QPoint pos = QCursor::pos();
    //pos = pobwindow->mapFromGlobal(pos);
    //lua_pushinteger(L, pos.x());
    //lua_pushinteger(L, pos.y());
    _ = L;
    return 0;
}

pub fn lSetCursorPos(L: *lj.c.lua_State) callconv(.C) c_int {
    //int n = lua_gettop(L);
    //LAssert(L, n >= 2, "Usage: SetCursorPos(x, y)");
    //LAssert(L, lua_isnumber(L, 1), "SetCursorPos() argument 1: expected number, got %t", 1);
    //LAssert(L, lua_isnumber(L, 2), "SetCursorPos() argument 2: expected number, got %t", 2);
    //pobwindow->sys->video->SetRelativeCursor((int)lua_tointeger(L, 1), (int)lua_tointeger(L, 2));
    _ = L;
    return 0;
}

pub fn lShowCursor(L: *lj.c.lua_State) callconv(.C) c_int {
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: ShowCursor(doShow)");
    ////pobwindow->sys->ShowCursor(lua_toboolean(L, 1));
    _ = L;
    return 0;
}

pub fn lIsKeyDown(L: *lj.c.lua_State) callconv(.C) c_int {
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: IsKeyDown(keyName)");
    //LAssert(L, lua_isstring(L, 1), "IsKeyDown() argument 1: expected string, got %t", 1);
    //size_t len;
    //const char* kname = lua_tolstring(L, 1, &len);
    //LAssert(L, len >= 1, "IsKeyDown() argument 1: string is empty", 1);
    //QString k(kname);
    //bool result = false;
    //if (k == "LEFTBUTTON") {
    //    if (QGuiApplication::mouseButtons() & Qt::LeftButton) {
    //        result = true;
    //    }
    //} else {
    //    int keys = QGuiApplication::keyboardModifiers();
    //    if (k == "CTRL") {
    //        result = keys & Qt::ControlModifier;
    //    } else if (k == "SHIFT") {
    //        result = keys & Qt::ShiftModifier;
    //    } else if (k == "ALT") {
    //        result = keys & Qt::AltModifier;
    //    } else {
    //        std::cout << "UNKNOWN ISKEYDOWN: " << k.toStdString() << std::endl;
    //    }
    //}
    //lua_pushboolean(L, result);
    ////int key = pobwindow->KeyForName(kname);
    ////LAssert(L, key, "IsKeyDown(): unrecognised key name");
    ////lua_pushboolean(L, pobwindow->sys->IsKeyDown(key));
    //return 1;
    _ = L;
    return 0;
}

pub fn lCopy(L: *lj.c.lua_State) callconv(.C) c_int {
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: Copy(string)");
    //LAssert(L, lua_isstring(L, 1), "Copy() argument 1: expected string, got %t", 1);
    //QGuiApplication::clipboard()->setText(lua_tostring(L, 1));
    _ = L;
    return 0;
}

pub fn lPaste(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //QString data = QGuiApplication::clipboard()->text();
    //if (data.size()) {
    //    lua_pushstring(L, data.toStdString().c_str());
    //    return 1;
    //} else {
    //    return 0;
    //}
}
