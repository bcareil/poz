const lj = @import("luajit");

pub fn lNewImageHandle(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lImgHandleGC(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lImgHandleLoad(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lImgHandleUnload(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lImgHandleIsValid(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lImgHandleIsLoading(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lImgHandleSetLoadingPriority(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lImgHandleImageSize(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lRenderInit(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lGetScreenSize(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSetClearColor(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSetDrawLayer(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSetViewport(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lSetDrawColor(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lDrawImage(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lDrawImageQuad(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lDrawString(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lDrawStringWidth(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lDrawStringCursorIndex(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
}

pub fn lStripEscapes(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: StripEscapes(string)");
    //LAssert(L, lua_isstring(L, 1), "StripEscapes() argument 1: expected string, got %t", 1);
    //const char* str = lua_tostring(L, 1);
    //char* strip = new char[strlen(str) + 1];
    //char* p = strip;
    //while (*str) {
    //    int esclen = IsColorEscape(str);
    //    if (esclen) {
    //        str+= esclen;
    //    } else {
    //        *(p++) = *(str++);
    //    }
    //}
    //*p = 0;
    //lua_pushstring(L, strip);
    //delete[] strip;
    //return 1;
}
