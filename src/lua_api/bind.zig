const std = @import("std");
const lj = @import("luajit");

const String = @import("../string.zig");

const lfs = @import("lua_fs.zig");
const lgfx = @import("lua_gfx.zig");
const linput = @import("lua_input.zig");
const lmisc = @import("lua_misc.zig");
const los = @import("lua_os.zig");
const lz = @import("lua_z.zig");

//#define ADDFUNC(n) lua_pushcclosure(L, l_##n, 0);lua_setglobal(L, #n);
//#define ADDFUNCCL(n, u) lua_pushcclosure(L, l_##n, u);lua_setglobal(L, #n);

const LuaCallback = fn (*lj.c.lua_State) callconv(.C) c_int;
const RealLuaCallback = fn (?*lj.c.lua_State) callconv(.C) c_int;

fn getLuaCallback(Module: anytype, comptime lua_fname: [:0]const u8) LuaCallback {
    const zig_fname = "l" ++ lua_fname;
    return @field(Module, zig_fname);
}

fn pushCClosure(L: *lj.c.lua_State, cb: LuaCallback, nupargs: c_int) !void {
    lj.c.lua_pushcclosure(L, @ptrCast(RealLuaCallback, cb), nupargs);
}

fn pushCFunction(L: *lj.c.lua_State, cb: LuaCallback) !void {
    return pushCClosure(L, @ptrCast(RealLuaCallback, cb), 0);
}

pub fn bindLuaClosure(
    L: *lj.c.lua_State,
    Module: anytype,
    comptime lua_fname: [:0]const u8,
    nupargs: c_int,
) !void {
    try pushCClosure(L, getLuaCallback(Module, lua_fname), nupargs);
    lj.c.lua_setglobal(L, lua_fname);
}

pub fn bindLuaCallback(L: *lj.c.lua_State, Module: anytype, comptime lua_fname: [:0]const u8) !void {
    return bindLuaClosure(L, Module, lua_fname, 0);
}

pub fn bindLuaCallbacks(L: *lj.c.lua_State) !void {
    // Callbacks
    lj.c.lua_newtable(L); // Callbacks table
    lj.c.lua_pushvalue(L, -1); // Push callbacks table
    try bindLuaClosure(L, lmisc, "SetCallback", 1);
    lj.c.lua_pushvalue(L, -1); // Push callbacks table
    try bindLuaClosure(L, lmisc, "GetCallback", 1);
    lj.c.lua_pushvalue(L, -1); // Push callbacks table
    try bindLuaClosure(L, lmisc, "SetMainObject", 1);
    lj.c.lua_setfield(L, lj.c.LUA_REGISTRYINDEX, "uicallbacks");

    // Image handles
    lj.c.lua_newtable(L); // Image handle metatable
    lj.c.lua_pushvalue(L, -1); // Push image handle metatable
    try bindLuaClosure(L, lgfx, "NewImageHandle", 1);
    lj.c.lua_pushvalue(L, -1); // Push image handle metatable
    lj.c.lua_setfield(L, -2, "__index");
    try pushCFunction(L, lgfx.lImgHandleGC);
    lj.c.lua_setfield(L, -2, "__gc");
    try pushCFunction(L, lgfx.lImgHandleLoad);
    lj.c.lua_setfield(L, -2, "Load");
    try pushCFunction(L, lgfx.lImgHandleUnload);
    lj.c.lua_setfield(L, -2, "Unload");
    try pushCFunction(L, lgfx.lImgHandleIsValid);
    lj.c.lua_setfield(L, -2, "IsValid");
    try pushCFunction(L, lgfx.lImgHandleIsLoading);
    lj.c.lua_setfield(L, -2, "IsLoading");
    try pushCFunction(L, lgfx.lImgHandleSetLoadingPriority);
    lj.c.lua_setfield(L, -2, "SetLoadingPriority");
    try pushCFunction(L, lgfx.lImgHandleImageSize);
    lj.c.lua_setfield(L, -2, "ImageSize");
    lj.c.lua_setfield(L, lj.c.LUA_REGISTRYINDEX, "uiimghandlemeta");

    // Rendering
    try bindLuaCallback(L, lgfx, "RenderInit");
    try bindLuaCallback(L, lgfx, "GetScreenSize");
    try bindLuaCallback(L, lgfx, "SetClearColor");
    try bindLuaCallback(L, lgfx, "SetDrawLayer");
    try bindLuaCallback(L, lgfx, "SetViewport");
    try bindLuaCallback(L, lgfx, "SetDrawColor");
    try bindLuaCallback(L, lgfx, "DrawImage");
    try bindLuaCallback(L, lgfx, "DrawImageQuad");
    try bindLuaCallback(L, lgfx, "DrawString");
    try bindLuaCallback(L, lgfx, "DrawStringWidth");
    try bindLuaCallback(L, lgfx, "DrawStringCursorIndex");
    try bindLuaCallback(L, lgfx, "StripEscapes");
    //try bindLuaCallback(L, lgfx, "GetAsyncCount");

    // Search handles
    lj.c.lua_newtable(L); // Search handle metatable
    lj.c.lua_pushvalue(L, -1); // Push search handle metatable
    try bindLuaClosure(L, lfs, "NewFileSearch", 1);
    lj.c.lua_pushvalue(L, -1); // Push search handle metatable
    lj.c.lua_setfield(L, -2, "__index");
    try pushCFunction(L, lfs.lSearchHandleGC);
    lj.c.lua_setfield(L, -2, "__gc");
    try pushCFunction(L, lfs.lSearchHandleNextFile);
    lj.c.lua_setfield(L, -2, "NextFile");
    try pushCFunction(L, lfs.lSearchHandleGetFileName);
    lj.c.lua_setfield(L, -2, "GetFileName");
    try pushCFunction(L, lfs.lSearchHandleGetFileSize);
    lj.c.lua_setfield(L, -2, "GetFileSize");
    try pushCFunction(L, lfs.lSearchHandleGetFileModifiedTime);
    lj.c.lua_setfield(L, -2, "GetFileModifiedTime");
    lj.c.lua_setfield(L, lj.c.LUA_REGISTRYINDEX, "uisearchhandlemeta");
}

/// For callbacks common to both the base application and "subscript" (aka
/// threads).
pub fn bindCommonLuaCallbacks(L: *lj.c.lua_State, allocator: std.mem.Allocator) !void {
    // LUA_PATH/LUA_CPATH
    {
        lj.c.lua_getfield(L, lj.c.LUA_GLOBALSINDEX, "package");
        lj.c.lua_getfield(L, -1, "path");

        var path = if (lj.c.lua_isstring(L, -1) != 0)
            try String.initFromCString(allocator, lj.c.lua_tolstring(L, -1, null))
        else
            try String.init(allocator);
        defer path.deinit();
        const my_path = "./PathOfBuilding/runtime/lua/?.lua;./PathOfBuilding/runtime/lua/?/init.lua";
        if (path.getSize() == 0) {
            try path.assignSlice(my_path);
        } else {
            try path.prependSlice(";");
            try path.prependSlice(my_path);
        }
        lj.c.lua_pushstring(L, path.asCSlice());
        lj.c.lua_setfield(L, -3, "path");

        lj.c.lua_pop(L, 2);
    }

    // General function
    try bindLuaCallback(L, lmisc, "SetWindowTitle");
    try bindLuaCallback(L, linput, "GetCursorPos");
    try bindLuaCallback(L, linput, "SetCursorPos");
    try bindLuaCallback(L, linput, "ShowCursor");
    try bindLuaCallback(L, linput, "IsKeyDown");
    try bindLuaCallback(L, linput, "Copy");
    try bindLuaCallback(L, linput, "Paste");
    try bindLuaCallback(L, lz, "Deflate");
    try bindLuaCallback(L, lz, "Inflate");
    try bindLuaCallback(L, los, "GetTime");
    try bindLuaCallback(L, lmisc, "GetScriptPath");
    try bindLuaCallback(L, lmisc, "GetRuntimePath");
    try bindLuaCallback(L, lmisc, "GetUserPath");
    try bindLuaCallback(L, lfs, "MakeDir");
    try bindLuaCallback(L, lfs, "RemoveDir");
    try bindLuaCallback(L, los, "SetWorkDir");
    try bindLuaCallback(L, los, "GetWorkDir");
    try bindLuaCallback(L, los, "LaunchSubScript");
    try bindLuaCallback(L, los, "AbortSubScript");
    try bindLuaCallback(L, los, "IsSubScriptRunning");
    try bindLuaCallback(L, lmisc, "LoadModule");
    try bindLuaCallback(L, lmisc, "PLoadModule");
    try bindLuaCallback(L, lmisc, "PCall");
    try bindLuaCallback(L, lmisc, "ConPrintTable");
    try bindLuaCallback(L, lmisc, "ConExecute");
    try bindLuaCallback(L, lmisc, "ConClear");
    //try bindLuaCallback(L, lmisc, "print");
    try bindLuaCallback(L, los, "SpawnProcess");
    try bindLuaCallback(L, los, "OpenURL");
    try bindLuaCallback(L, lmisc, "SetProfiling");
    try bindLuaCallback(L, los, "Restart");
    try bindLuaCallback(L, los, "Exit");

    //lj.c.lua_getglobal(L, "string");
    //lj.c.lua_getfield(L, -1, "format");
    ////ADDFUNCCL(ConPrintf, 1);
    //lj.c.lua_pop(L, 1);		// Pop 'string' table

    //lj.c.lua_getglobal(L, "os");
    //try pushCFunction(L, l_Exit);
    //lj.c.lua_setfield(L, -2, "exit");
    //lj.c.lua_pop(L, 1);		// Pop 'os' table
}

test "Generics" {
    try std.testing.expectEqual(getLuaCallback(lgfx, "DrawImage"), lgfx.lDrawImage);
}
