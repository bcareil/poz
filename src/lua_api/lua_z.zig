const lj = @import("luajit");

pub fn lDeflate(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: Deflate(string)");
    //LAssert(L, lua_isstring(L, 1), "Deflate() argument 1: expected string, got %t", 1);
    //z_stream_s z;
    //z.zalloc = NULL;
    //z.zfree = NULL;
    //deflateInit(&z, 9);
    //size_t inLen;
    //Byte* in = (Byte*)lua_tolstring(L, 1, &inLen);
    //uLong outSz = deflateBound(&z, inLen);
    //std::vector<Byte> out(outSz);
    //z.next_in = in;
    //z.avail_in = inLen;
    //z.next_out = out.data();
    //z.avail_out = outSz;
    //int err = deflate(&z, Z_FINISH);
    //deflateEnd(&z);
    //if (err == Z_STREAM_END) {
    //    lua_pushlstring(L, (const char*)out.data(), z.total_out);
    //    return 1;
    //} else {
    //    lua_pushnil(L);
    //    lua_pushstring(L, zError(err));
    //    return 2;
    //}
}

pub fn lInflate(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: Inflate(string)");
    //LAssert(L, lua_isstring(L, 1), "Inflate() argument 1: expected string, got %t", 1);
    //size_t inLen;
    //Byte* in = (Byte*)lua_tolstring(L, 1, &inLen);
    //int outSz = inLen * 4;
    //Byte* out = new Byte[outSz];
    //z_stream_s z;
    //z.next_in = in;
    //z.avail_in = inLen;
    //z.zalloc = NULL;
    //z.zfree = NULL;
    //z.next_out = out;
    //z.avail_out = outSz;
    //inflateInit(&z);
    //int err;
    //while ((err = inflate(&z, Z_NO_FLUSH)) == Z_OK) {
    //    if (z.avail_out == 0) {
    //        // Output buffer filled, embiggen it
    //        int newSz = outSz << 1;
    //        Byte *newOut = (Byte *)realloc(out, newSz);
    //        if (newOut) {
    //            out = newOut;
    //        } else {
    //            // PANIC
    //            delete[] out;
    //            return 0;
    //        }
    //        z.next_out = out + outSz;
    //        z.avail_out = outSz;
    //        outSz = newSz;
    //    }
    //  }
    //inflateEnd(&z);
    //if (err == Z_STREAM_END) {
    //    lua_pushlstring(L, (const char*)out, z.total_out);
    //    return 1;
    //} else {
    //    lua_pushnil(L);
    //    lua_pushstring(L, zError(err));
    //    return 2;
    //}
}
