const lj = @import("luajit");

pub fn lGetTime(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //qint64 ms = QDateTime::currentDateTime().toMSecsSinceEpoch();
    //lua_pushinteger(L, ms);
    //return 1;
}

pub fn lSetWorkDir(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: SetWorkDir(path)");
    //LAssert(L, lua_isstring(L, 1), "SetWorkDir() argument 1: expected string, got %t", 1);
    //if (QDir::setCurrent(lua_tostring(L, 1))) {
    //    pobwindow->scriptWorkDir = lua_tostring(L, 1);
    //}
    //return 0;
}

pub fn lGetWorkDir(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //lua_pushstring(L, QDir::currentPath().toStdString().c_str());
    //return 1;
}

pub fn lLaunchSubScript(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 3, "Usage: LaunchSubScript(scriptText, funcList, subList[, ...])");
    //for (int i = 1; i <= 3; i++) {
    //    LAssert(L, lua_isstring(L, i), "LaunchSubScript() argument %d: expected string, got %t", i, i);
    //}
    //for (int i = 4; i <= n; i++) {
    //    LAssert(L, lua_isnil(L, i) || lua_isboolean(L, i) || lua_isnumber(L, i) || lua_isstring(L, i),
    //                       "LaunchSubScript() argument %d: only nil, boolean, number and string types can be passed to sub script", i);
    //}
    //int slot = pobwindow->subScriptList.size();
    //pobwindow->subScriptList.append(std::make_shared<SubScript>(L));
    //// Signal us when the subscript completes so we can trigger a repaint.
    //pobwindow->connect( pobwindow->subScriptList[slot].get(), &SubScript::finished, pobwindow, &POBWindow::subScriptFinished );
    //pobwindow->subScriptList[slot]->start();
    //lua_pushinteger(L, slot);
    //return 1;
}

pub fn lAbortSubScript(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //std::cout << "SUBSCRIPT ABORT STUB" << std::endl;
    //return 0;
    // /*
    //    int n = lua_gettop(L);
    //  LAssert(L, n >= 1, "Usage: AbortSubScript(ssID)");
    //  LAssert(L, lua_islightuserdata(L, 1), "AbortSubScript() argument 1: expected subscript ID, got %t", 1);
    //  notdword slot = (notdword)lua_touserdata(L, 1);
    //  LAssert(L, slot < pobwindow->subScriptSize && pobwindow->subScriptList[slot], "AbortSubScript() argument 1: invalid subscript ID");
    //  LAssert(L, pobwindow->subScriptList[slot]->IsRunning(), "AbortSubScript(): subscript isn't running");
    //  ui_ISubScript::FreeHandle(pobwindow->subScriptList[slot]);
    //  pobwindow->subScriptList[slot] = NULL;
    //  return 0;
    //*/
}

pub fn lIsSubScriptRunning(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    // std::cout << "SUBSCRIPT RUNNING STUB" << std::endl;
    // return 0;
    // /*
    //     int n = lua_gettop(L);
    //   LAssert(L, n >= 1, "Usage: IsSubScriptRunning(ssID)");
    //   LAssert(L, lua_islightuserdata(L, 1), "IsSubScriptRunning() argument 1: expected subscript ID, got %t", 1);
    //   notdword slot = (notdword)lua_touserdata(L, 1);
    //   LAssert(L, slot < pobwindow->subScriptSize && pobwindow->subScriptList[slot], "IsSubScriptRunning() argument 1: invalid subscript ID");
    //   lua_pushboolean(L, pobwindow->subScriptList[slot]->IsRunning());
    //   return 1;
    // */
}

pub fn lSpawnProcess(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: SpawnProcess(cmdName[, args])");
    //LAssert(L, lua_isstring(L, 1), "SpawnProcess() argument 1: expected string, got %t", 1);
    //// FIXME
    //  //  pobwindow->sys->SpawnProcess(lua_tostring(L, 1), lua_tostring(L, 2));
    //return 0;
}

pub fn lOpenURL(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //LAssert(L, n >= 1, "Usage: OpenURL(url)");
    //LAssert(L, lua_isstring(L, 1), "OpenURL() argument 1: expected string, got %t", 1);
    //// FIXME
    ////pobwindow->sys->OpenURL(lua_tostring(L, 1));
    //return 0;
}

pub fn lRestart(L: *lj.c.lua_State) callconv(.C) c_int {
    // FIXME
    //pobwindow->restartFlag = true;
    _ = L;
    return 0;
}

pub fn lExit(L: *lj.c.lua_State) callconv(.C) c_int {
    _ = L;
    return 0;
    //int n = lua_gettop(L);
    //const char* msg = nullptr;
    //if (n >= 1 && !lua_isnil(L, 1)) {
    //    LAssert(L, lua_isstring(L, 1), "Exit() argument 1: expected string or nil, got %t", 1);
    //    msg = lua_tostring(L, 1);
    //}
    //(void)msg;
    //// FIXME
    ////pobwindow->sys->Exit(msg);
    ////pobwindow->didExit = true;
    //  //    lua_pushstring(L, "dummy");
    //  //    lua_error(L);
    //return 0;
}
