const std = @import("std");
const ft = @import("freetype");
const String = @import("string.zig");
const Font = @import("font.zig");

const FontManager = @This();

const FontEntry = struct {
    name: String,
    font: Font,
};

allocator: std.mem.Allocator,
ft_lib: ft.c.FT_Library,
fonts: std.MultiArrayList(FontEntry),
font_lookup: std.StringHashMap(FontId),

pub const FontId = u32;

pub const Error = error{
    FreeTypeInitError,
    LoadFontError,
    LoadGlyphError,
    SetPixelSizeError,
    FontNameAlreadyUsed,
};

pub fn init(allocator: std.mem.Allocator) !FontManager {
    var lib: ft.c.FT_Library = undefined;
    const err = ft.c.FT_Init_FreeType(&lib);
    if (err != 0) {
        return Error.FreeTypeInitError;
    }
    return FontManager{
        .allocator = allocator,
        .ft_lib = lib,
        .fonts = std.MultiArrayList(FontEntry){},
        .font_lookup = std.StringHashMap(FontId).init(allocator),
    };
}

pub fn deinit(self: *FontManager) void {
    self.fonts.deinit(self.allocator);
    self.font_lookup.deinit();
}

pub fn load(self: *FontManager, name: []const u8, path: []const u8) !FontId {
    var name_ = try String.initCopy(self.allocator, name);
    errdefer name_.deinit();
    const font_id = @intCast(FontId, self.fonts.len);
    const lookup_entry = try self.font_lookup.getOrPutValue(name_.asSlice(), font_id);
    if (lookup_entry.value_ptr.* != font_id) {
        return Error.FontNameAlreadyUsed;
    }
    errdefer _ = self.font_lookup.remove(name_.asSlice());
    var c_path = try self.allocator.allocWithOptions(u8, path.len, null, 0);
    std.mem.copy(u8, c_path, path);
    var font = blk: {
        var face: ft.c.FT_Face = undefined;
        const err = ft.c.FT_New_Face(self.ft_lib, c_path, 0, &face);
        if (err != 0) {
            return Error.LoadFontError;
        }
        errdefer ft.c.FT_Free_Face(face);
        break :blk Font.init(self.allocator, face);
    };
    errdefer font.deinit();
    try self.fonts.append(self.allocator, .{
        .name = name_,
        .font = font,
    });
    return font_id;
}

pub fn get(self: *FontManager, id: FontId) ?*Font {
    if (id < self.fonts.len) {
        return &self.fonts.items(.font)[id];
    }
    return null;
}

pub fn getId(self: *FontManager, name: []const u8) ?FontId {
    return self.font_lookup.get(name);
}
