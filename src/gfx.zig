const std = @import("std");
const mach = @import("mach");
const gpu = @import("gpu");
const zm = @import("zmath");

const String = @import("string.zig");
const TextureManager = @import("texture_manager.zig");
const FontManager = @import("font_manager.zig");
const TextRenderer = @import("gfx/text.zig").TextRenderer;
const types = @import("types.zig");

const BBox = types.BBox;
const FontId = FontManager.FontId;
const TexId = TextureManager.TexId;

pub const Vertex = packed struct {
    pos: @Vector(4, f32),
    uv: @Vector(2, f32),
};

pub const Commands = struct {
    const Self = @This();
    const Kind = enum {
        Image,
        Text,
    };

    const CommandIndex = struct {
        kind: Kind,
        index: usize,
    };
    const CommandIndices = std.MultiArrayList(CommandIndex);

    pub const DrawImageCommand = struct {
        texture: TexId,
        position: @Vector(2, f32),
        size: @Vector(2, f32),
    };
    const DrawImageCommands = std.MultiArrayList(DrawImageCommand);

    pub const DrawTextCommand = struct {
        text: String,
        alignment: Alignment,
        position: @Vector(2, f32),
        font_size: u32,
        font_family: FontId,

        pub const Alignment = enum {
            Left,
            Center,
            Right,
        };
    };
    const DrawTextCommands = std.MultiArrayList(DrawTextCommand);

    allocator: std.mem.Allocator,
    indices: CommandIndices = .{},
    cmd_draw_image: DrawImageCommands = .{},
    cmd_draw_text: DrawTextCommands = .{},

    pub fn init(allocator: std.mem.Allocator) Self {
        return Self{ .allocator = allocator };
    }

    pub fn deinit(self: *Self) void {
        self.indices.deinit(self.allocator);
        self.cmd_draw_image.deinit(self.allocator);
        self.cmd_draw_text.deinit(self.allocator);
    }

    pub fn addDrawImage(self: *Self, cmd: DrawImageCommand) !void {
        try self.indices.append(self.allocator, .{ .kind = .Image, .index = self.cmd_draw_image.len });
        errdefer self.indices.shrinkRetainingCapacity(self.indices.len - 1);
        try self.cmd_draw_image.append(self.allocator, cmd);
    }

    pub fn addDrawText(self: *Self, cmd: DrawTextCommand) !void {
        try self.indices.append(self.allocator, .{ .kind = .Text, .index = self.cmd_draw_text.len });
        errdefer self.indices.shrinkRetainingCapacity(self.indices.len - 1);
        try self.cmd_draw_text.append(self.allocator, cmd);
    }

    pub fn clear(self: *Self) void {
        self.indices.shrinkRetainingCapacity(0);
        self.cmd_draw_image.shrinkRetainingCapacity(0);
        self.cmd_draw_text.shrinkRetainingCapacity(0);
    }
};

const VertexBuffer = struct {
    buffer: gpu.Buffer,
    array: std.ArrayList(Vertex),
    capacity: usize,

    const Self = @This();

    pub fn init(device: *gpu.Device, allocator: std.mem.Allocator) !Self {
        const capacity = 10_000;
        const array = try std.ArrayList(Vertex).initCapacity(allocator, capacity);
        const buffer = device.createBuffer(&.{
            .usage = .{
                .vertex = true,
                .copy_dst = true,
            },
            .size = @sizeOf(Vertex) * capacity,
            .mapped_at_creation = false,
        });
        return Self{
            .buffer = buffer,
            .array = array,
            .capacity = capacity,
        };
    }

    pub fn deinit(self: *Self) void {
        self.buffer.release();
        self.array.deinit();
    }

    pub fn clear(self: *Self) void {
        self.array.clearRetainingCapacity();
    }

    pub fn append(self: *Self, v: Vertex) !void {
        if ((self.array.items.len + 1) >= self.capacity) {
            return error.CapacityError;
        }
        self.array.appendAssumeCapacity(v);
    }
};

pub const SpriteBatch = struct {
    vertex_buffer: VertexBuffer,
    entries: std.ArrayList(Entry),

    const Self = @This();
    const Entry = struct {
        first: usize,
        last: usize,
        tex_id: TextureManager.TexId,
    };

    pub fn init(device: *gpu.Device, allocator: std.mem.Allocator) !Self {
        var vertex_buffer = try VertexBuffer.init(device, allocator);
        errdefer vertex_buffer.deinit();
        var entries = try std.ArrayList(Entry).initCapacity(allocator, 16);
        errdefer entries.deinit();

        return Self{
            .vertex_buffer = vertex_buffer,
            .entries = entries,
        };
    }

    pub fn deinit(self: *Self) void {
        self.vertex_buffer.deinit();
        self.entries.deinit();
    }

    pub fn clear(self: *Self) void {
        self.vertex_buffer.clear();
        self.entries.shrinkRetainingCapacity(0);
    }

    pub fn pushSpritePDUV(self: *Self, t: TextureManager.TexId, p: @Vector(2, f32), d: @Vector(2, f32), uv: BBox(f32)) !void {
        var count_pushed: usize = 0;
        errdefer self.vertex_buffer.array.shrinkRetainingCapacity(self.vertex_buffer.array.items.len - count_pushed);
        try self.vertex_buffer.append(.{
            .pos = .{ p[0], p[1], 0.0, 1.0 },
            .uv = .{ uv.left, uv.top },
        });
        count_pushed += 1;
        try self.vertex_buffer.append(.{
            .pos = .{ p[0] + d[0], p[1], 0.0, 1.0 },
            .uv = .{ uv.right, uv.top },
        });
        count_pushed += 1;
        try self.vertex_buffer.append(.{
            .pos = .{ p[0], p[1] + d[1], 0.0, 1.0 },
            .uv = .{ uv.left, uv.bottom },
        });
        count_pushed += 1;

        try self.vertex_buffer.append(.{
            .pos = .{ p[0], p[1] + d[1], 0.0, 1.0 },
            .uv = .{ uv.left, uv.bottom },
        });
        count_pushed += 1;
        try self.vertex_buffer.append(.{
            .pos = .{ p[0] + d[0], p[1] + d[1], 0.0, 1.0 },
            .uv = .{ uv.right, uv.bottom },
        });
        count_pushed += 1;
        try self.vertex_buffer.append(.{
            .pos = .{ p[0] + d[0], p[1], 0.0, 1.0 },
            .uv = .{ uv.right, uv.top },
        });
        count_pushed += 1;

        if (self.entries.items.len != 0 and self.entries.items[self.entries.items.len - 1].tex_id == t) {
            self.entries.items[self.entries.items.len - 1].last += count_pushed;
        } else {
            if (self.entries.addOne()) |e| {
                e.* = .{
                    .first = self.vertex_buffer.array.items.len - count_pushed,
                    .last = self.vertex_buffer.array.items.len,
                    .tex_id = t,
                };
            } else |_| {
                @panic("no more memory");
            }
        }
    }

    pub fn pushSpritePD(self: *Self, t: TextureManager.TexId, p: @Vector(2, f32), d: @Vector(2, f32)) !void {
        try self.pushSpritePDUV(t, p, d, BBox(f32){
            .left = 0.0,
            .top = 0.0,
            .right = 1.0,
            .bottom = 1.0,
        });
    }

    pub fn uploadVertices(self: *Self, device: *gpu.Device) void {
        device.getQueue().writeBuffer(self.vertex_buffer.buffer, 0, Vertex, self.vertex_buffer.array.items);
    }
};

pub const Renderer = struct {
    sampler: gpu.Sampler,
    transform_buffer: gpu.Buffer,
    global_bind_group: gpu.BindGroup,
    default_texture_bind_group: gpu.BindGroup,
    texture_bind_groups: std.ArrayList(?gpu.BindGroup),
    text_renderer: TextRenderer,
    sprite_batch: SpriteBatch,

    const Self = @This();

    pub fn init(
        device: *gpu.Device,
        pipeline: *gpu.RenderPipeline,
        texture_manager: *TextureManager,
        font_manager: *FontManager,
        allocator: std.mem.Allocator,
    ) !Self {
        // Fragment shader sampler
        const sampler_desc: gpu.Sampler.Descriptor = .{
            .address_mode_u = .repeat,
            .address_mode_v = .repeat,
            .address_mode_w = .repeat,
            .mag_filter = .linear,
            .min_filter = .linear,
            .mipmap_filter = .linear,
        };
        const sampler = device.createSampler(&sampler_desc);
        errdefer sampler.release();

        // Vertex shader uniform
        const transform_buffer_desc = .{
            .usage = .{
                .uniform = true,
                .copy_dst = true,
            },
            .size = 16 * 4,
            .mapped_at_creation = false,
        };
        const transform_buffer =
            device.createBuffer(&transform_buffer_desc);
        errdefer transform_buffer.release();

        // First bind group: global transform and sampler
        const bind_group_desc: gpu.BindGroup.Descriptor = .{
            .layout = pipeline.getBindGroupLayout(0),
            .entries = &.{
                gpu.BindGroup.Entry.buffer(0, transform_buffer, 0, transform_buffer_desc.size),
                gpu.BindGroup.Entry.sampler(1, sampler),
            },
        };
        const bind_group = device.createBindGroup(&bind_group_desc);
        errdefer bind_group.release();

        // Second, default, bind group: texture
        const default_texture_bind_group_desc: gpu.BindGroup.Descriptor = .{
            .layout = pipeline.getBindGroupLayout(1),
            .entries = &.{
                gpu.BindGroup.Entry.textureView(0, texture_manager.getDefaultTexture().createView(&.{
                    .dimension = .dimension_2d,
                    .mip_level_count = 1,
                    .array_layer_count = 1,
                })),
            },
        };
        const default_texture_bind_group = device.createBindGroup(&default_texture_bind_group_desc);
        errdefer default_texture_bind_group.release();

        var sprite_batch = try SpriteBatch.init(device, allocator);
        errdefer sprite_batch.deinit();

        var text_renderer = try TextRenderer.init(font_manager, texture_manager, allocator);
        errdefer text_renderer.deinit();

        return Self{
            .sampler = sampler,
            .transform_buffer = transform_buffer,
            .global_bind_group = bind_group,
            .default_texture_bind_group = default_texture_bind_group,
            .texture_bind_groups = try std.ArrayList(?gpu.BindGroup).initCapacity(allocator, 16),
            .text_renderer = text_renderer,
            .sprite_batch = sprite_batch,
        };
    }

    pub fn deinit(self: *Self) void {
        for (self.texture_bind_groups.items) |e| {
            if (e) |bg| {
                bg.release();
            }
        }
        self.sampler.release();
        self.transform_buffer.release();
        self.global_bind_group.release();
        self.default_texture_bind_group.release();
        self.texture_bind_groups.deinit();
        self.sprite_batch.deinit();
    }

    pub fn getTextureBindGroup(
        self: *Self,
        pipeline: *gpu.RenderPipeline,
        device: *gpu.Device,
        texture_manager: *TextureManager,
        tex_id: TextureManager.TexId,
    ) *gpu.BindGroup {
        while (tex_id >= self.texture_bind_groups.items.len) {
            if (self.texture_bind_groups.addOne()) |bg| {
                bg.* = null;
            } else |_| {
                @panic("no more memory");
            }
        }
        if (self.texture_bind_groups.items[tex_id]) |*bg| {
            return bg;
        }
        if (texture_manager.getNoDefault(tex_id)) |tex| {
            const desc = gpu.BindGroup.Descriptor{
                .layout = pipeline.getBindGroupLayout(1),
                .entries = &.{
                    gpu.BindGroup.Entry.textureView(0, tex.texture.createView(&.{
                        .dimension = .dimension_2d,
                        .mip_level_count = 1,
                        .array_layer_count = 1,
                    })),
                },
            };
            self.texture_bind_groups.items[tex_id] = device.createBindGroup(&desc);
            return &self.texture_bind_groups.items[tex_id].?;
        }
        return &self.default_texture_bind_group;
    }

    pub fn render(
        self: *Self,
        pipeline: *gpu.RenderPipeline,
        device: *gpu.Device,
        pass: *gpu.RenderPassEncoder,
        texture_manager: *TextureManager,
        commands: *const Commands,
    ) void {
        // Process commands
        blk_cmd: {
            self.sprite_batch.clear();
            const s_cdi = commands.cmd_draw_image.slice();
            const img_texture = s_cdi.items(.texture);
            const img_position = s_cdi.items(.position);
            const img_size = s_cdi.items(.size);
            const s_cmd = commands.indices.slice();
            const cmd_index = s_cmd.items(.index);
            const cmd_kind = s_cmd.items(.kind);
            for (cmd_kind) |k, i| {
                switch (k) {
                    .Image => {
                        const index = cmd_index[i];
                        const t = img_texture[index];
                        const p = img_position[index];
                        const s = img_size[index];
                        self.sprite_batch.pushSpritePD(t, p, s) catch {
                            std.log.warn("No more vertices", .{});
                            break :blk_cmd;
                        };
                    },
                    .Text => {
                        const index = cmd_index[i];
                        const cmd = commands.cmd_draw_text.get(index);
                        self.text_renderer.render(&self.sprite_batch, &cmd) catch {
                            std.log.warn("No more vertices for text", .{});
                            break :blk_cmd;
                        };
                    },
                }
            }
        }

        // Update uniform
        {
            const w = 640.0;
            const h = 480.0;
            const proj = zm.Mat{
                .{ 2.0 / w, 0.0, 0.0, -1.0 },
                .{ 0.0, -2.0 / h, 0.0, 1.0 },
                .{ 0.0, 0.0, 2.0 / 1.0, 0.0 },
                .{ 0.0, 0.0, 0.0, 1.0 },
            };
            device.getQueue().writeBuffer(self.transform_buffer, 0, zm.Vec, &proj);
        }

        // Update vertices
        self.sprite_batch.uploadVertices(device);
        self.text_renderer.preFrame(texture_manager);

        pass.setVertexBuffer(0, self.sprite_batch.vertex_buffer.buffer, 0, @sizeOf(Vertex) * self.sprite_batch.vertex_buffer.array.items.len);
        pass.setBindGroup(0, self.global_bind_group, &.{});

        for (self.sprite_batch.entries.items) |e| {
            const num = e.last - e.first;
            const bind_group = self.getTextureBindGroup(pipeline, device, texture_manager, e.tex_id);
            pass.setBindGroup(1, bind_group.*, &.{});
            pass.draw(@intCast(u32, num), 1, @intCast(u32, e.first), 0);
        }
    }
};
