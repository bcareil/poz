const TextureManager = @import("texture_manager.zig");
const types = @import("types.zig");
const gfx = @import("gfx.zig");

const Dimensions = types.Dimensions;
const DrawImageCommand = gfx.Commands.DrawImageCommand;

const Texture = struct {
    texture_manager: *TextureManager,
    path: []const u8,
    id: u32,
    size: Dimensions,

    pub fn init(texture_manager: *TextureManager, path_: []const u8) Texture {
        var tex = texture_manager.load(path_);
        return Texture{
            .texture_manager = texture_manager,
            .path = tex.path,
            .id = tex.id,
            .size = tex.size,
        };
    }

    pub fn destroy(self: *Texture) void {
        self.texture_manager.release(self.id);
    }

    pub fn draw(self: *Texture, x: i32, y: i32) DrawImageCommand {
        return DrawImageCommand{
            .texture = self.id,
            .position = .{ @intToFloat(f32, x), @intToFloat(f32, y) },
            .size = .{ 1, 1 },
        };
    }
};
