const std = @import("std");
const ft = @import("freetype");
const types = @import("types.zig");
const bm = @import("bitmap.zig");

const BBox = types.BBox;
const Dimensions = types.Dimensions;
const BitmapUnmanaged = bm.BitmapUnmanaged;

const Font = @This();

const Error = @import("font_manager.zig").Error;

const GlyphOffsets = struct {
    offset: @Vector(2, f32),
    advance: @Vector(2, f32),
};

const GlyphEntry = struct {
    codepoint: u32,
    index: usize,
    offsets: GlyphOffsets,
    dimensions: Dimensions,
};

allocator: std.mem.Allocator,
face: ft.c.FT_Face,
bitmaps: std.ArrayList(u8),
glyphs: std.MultiArrayList(GlyphEntry),
glyph_lookup: std.AutoHashMap(u32, usize),

pub const GlyphRef = struct {
    pixels: BitmapUnmanaged(u8),
    offsets: GlyphOffsets,
};

pub fn init(allocator: std.mem.Allocator, face: ft.c.FT_Face) Font {
    return Font{
        .allocator = allocator,
        .face = face,
        .bitmaps = std.ArrayList(u8).init(allocator),
        .glyphs = std.MultiArrayList(GlyphEntry){},
        .glyph_lookup = std.AutoHashMap(u32, usize).init(allocator),
    };
}

pub fn deinit(self: *Font) void {
    self.bitmaps.deinit();
    self.glyphs.deinit(self.allocator);
    self.glyph_lookup.deinit();
}

fn makeGlyphRef(self: *Font, id: usize) GlyphRef {
    const slice = self.glyphs.slice();
    const index = slice.items(.index)[id];
    const offsets = slice.items(.offsets)[id];
    const dimensions = slice.items(.dimensions)[id];
    const bitmap_size = dimensions.w * dimensions.h;
    const bitmap_slice = self.bitmaps.items[index..(index + bitmap_size)];
    return GlyphRef{
        .pixels = BitmapUnmanaged(u8).init(bitmap_slice, dimensions.w, dimensions.h),
        .offsets = offsets,
    };
}

pub fn getGlyph(self: *Font, codepoint: u32, size: u32) !GlyphRef {
    const new_glyph_id = self.glyphs.len;
    const lookup_entry = try self.glyph_lookup.getOrPutValue(codepoint, new_glyph_id);
    if (lookup_entry.value_ptr.* != new_glyph_id) {
        return self.makeGlyphRef(lookup_entry.value_ptr.*);
    }
    errdefer _ = self.glyph_lookup.remove(codepoint);

    var err = ft.c.FT_Set_Pixel_Sizes(self.face, 0, size);
    if (err != 0) {
        return Error.SetPixelSizeError;
    }

    err = ft.c.FT_Load_Char(self.face, codepoint, ft.c.FT_LOAD_RENDER);
    if (err != 0) {
        return Error.LoadGlyphError;
    }
    const glyph = self.face.*.glyph;
    const width = glyph.*.bitmap.width;
    const height = glyph.*.bitmap.rows;
    const pitch = @intCast(c_uint, glyph.*.bitmap.pitch); // TODO: handle negative values
    const bitmap: []const u8 = if (glyph.*.bitmap.buffer != null) @ptrCast([*]const u8, glyph.*.bitmap.buffer)[0 .. height * pitch] else &.{0};

    const glyph_entry = GlyphEntry{
        .codepoint = codepoint,
        .index = self.bitmaps.items.len,
        .offsets = .{
            .offset = .{
                @intToFloat(f32, glyph.*.bitmap_left),
                @intToFloat(f32, glyph.*.bitmap_top),
            },
            .advance = .{
                @intToFloat(f32, glyph.*.advance.x >> 6),
                @intToFloat(f32, glyph.*.advance.y >> 6),
            },
        },
        .dimensions = .{ .w = width, .h = height },
    };
    _ = glyph_entry;

    try self.bitmaps.ensureUnusedCapacity(bitmap.len);
    errdefer self.bitmaps.shrinkRetainingCapacity(glyph_entry.index);
    var i: usize = 0;
    var y: usize = 0;
    while (y < height) : (y += 1) {
        var x: usize = 0;
        while (x < width) : (x += 1) {
            self.bitmaps.appendAssumeCapacity(bitmap[x + i]);
        }
        i += pitch;
    }

    try self.glyphs.append(self.allocator, glyph_entry);
    errdefer self.glyphs.shrinkRetainingCapacity(self.glyphs.len - 1);

    return self.makeGlyphRef(new_glyph_id);
}
