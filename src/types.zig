pub const Dimensions = struct {
    w: u32,
    h: u32,
};

pub fn BBox(T: anytype) type {
    return struct {
        left: T,
        top: T,
        right: T,
        bottom: T,
    };
}
