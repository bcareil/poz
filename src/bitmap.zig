const std = @import("std");

pub fn BitmapUnmanaged(CellT: anytype) type {
    return struct {
        width: usize,
        height: usize,
        buffer: []CellT,

        const Self = @This();

        pub fn init(buffer: []CellT, width: usize, height: usize) Self {
            std.debug.assert(buffer.len == (width * height));
            return Self{
                .width = width,
                .height = height,
                .buffer = buffer,
            };
        }

        pub fn deinit(self: *Self) void {
            _ = self;
        }

        pub fn clear(self: *Self) void {
            for (self.buffer) |*b| {
                b.* = 0;
            }
        }

        pub fn put(self: *Self, x: usize, y: usize, b: CellT) void {
            if (x >= self.width or y >= self.height) {
                return;
            }
            self.buffer[x + y * self.width] = b;
        }

        pub fn get(self: *const Self, x: usize, y: usize) CellT {
            if (x >= self.width or y >= self.height) {
                return 0;
            }
            return self.buffer[x + y * self.width];
        }
    };
}

pub fn Bitmap(CellT: anytype) type {
    return struct {
        allocator: std.mem.Allocator,
        bitmap: BitmapUnmanaged(CellT),

        const Self = @This();

        pub fn init(allocator: std.mem.Allocator, width: usize, height: usize) !Self {
            var self = Self{
                .allocator = allocator,
                .bitmap = BitmapUnmanaged(CellT).init(
                    try allocator.alloc(CellT, width * height),
                    width,
                    height,
                ),
            };
            self.clear();
            return self;
        }

        pub fn deinit(self: *Self) void {
            self.bitmap.deinit();
            self.allocator.free(self.buffer);
        }

        pub fn clear(self: *Self) void {
            self.bitmap.clear();
        }

        pub fn put(self: *Self, x: usize, y: usize, b: CellT) void {
            self.bitmap.put(x, y, b);
        }

        pub fn get(self: *Self, x: usize, y: usize) CellT {
            return self.bitmap.get(x, y);
        }
    };
}
