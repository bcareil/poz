@group(0) @binding(1) var mySampler: sampler;
@group(1) @binding(0) var myTexture: texture_2d<f32>;

@stage(fragment)
fn main(@location(0) fragUV: vec2<f32>,
        @location(1) fragPosition: vec4<f32>) -> @location(0) vec4<f32> {
    return textureSample(myTexture, mySampler, fragUV);// * vec4<f32>(fragUV, 0.5, 2.0) + fragPosition);
}
