const std = @import("std");

const luajit = @import("third_party/zig-luajit/build.zig");
const addLuajit = luajit.addLuajit;
const luajit_pkg = luajit.pkg;

const freetype = @import("third_party/zig-freetype/build.zig");
const addFreetype = freetype.addFreetype;
const freetype_pkg = freetype.pkg;

const zmath = @import("third_party/zmath/build.zig");

const mach = @import("third_party/mach/build.zig");
const mach_pkg = mach.pkg;
const gpu_pkg = mach.gpu.pkg;
const glfw_pkg = mach.glfw.pkg;

const zigimg_pkg = std.build.Pkg{
    .name = "zigimg",
    .path = .{ .path = "third_party/zigimg/zigimg.zig" },
    .dependencies = &.{},
};

pub fn build(b: *std.build.Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const glfw_options = mach.glfw.Options{};
    const gpu_options = mach.gpu.Options{};

    const exe = b.addExecutable("poz", "third_party/mach/src/native.zig");
    addLuajit(exe) catch unreachable;
    addFreetype(exe) catch unreachable;
    exe.addPackage(.{
        .name = "app",
        .path = .{ .path = "src/main.zig" },
        .dependencies = &.{
            glfw_pkg,
            gpu_pkg,
            mach_pkg,
            zmath.pkg,
            zigimg_pkg,
            luajit_pkg,
            freetype_pkg,
        },
    });
    exe.addPackage(gpu_pkg);
    exe.addPackage(glfw_pkg);
    mach.glfw.link(b, exe, glfw_options);
    mach.gpu.link(b, exe, gpu_options);
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    run_cmd.cwd = "./pkg";

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const test_sources = [_][]const u8{
        "src/string.zig",
        "src/lua_api/bind.zig",
    };
    const test_step = b.step("test", "Run unit tests");
    for (test_sources) |s| {
        const exe_tests = b.addTest(s);
        exe_tests.setTarget(target);
        exe_tests.setBuildMode(mode);
        addLuajit(exe_tests) catch unreachable;
        test_step.dependOn(&exe_tests.step);
    }
}
